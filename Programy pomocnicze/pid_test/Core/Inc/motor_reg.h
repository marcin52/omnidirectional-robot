/*
 * motor_reg.h
 *
 *  Created on: Jun 15, 2021
 *      Author: marci
 */

#ifndef INC_MOTOR_REG_H_
#define INC_MOTOR_REG_H_

#include "main.h"
#include <stdbool.h>

#include "encoder.h"
#include "dc_motor.h"

typedef struct{
	double Kp;
	double Ki;
	double Kd;
	double dt;
	double error;
	double errorInt;
	double setVelocity;
	double encVelocity;
	double maxIntVal;
	double derivative;
	double pre_error;
}PidReg_t;

typedef struct{
	EncoderImplementation_t encImplementation;
	EncoderSoftware_t * encSoftware;
	EncoderHardware_t * encHardware;
	DcMotor_t * motor;
	PidReg_t * pid;
}MotorReg_t;

PidReg_t MotorReg_CreatePid(double dt, double maxInt, double Kp, double Ki, double Kd);

MotorReg_t MotorReg_Create(PidReg_t * pidReg, DcMotor_t * motor,
		EncoderImplementation_t encImple, EncoderHardware_t * encHard,
		EncoderSoftware_t * encSoft);

void MotorReg_Destroy(MotorReg_t * regulator);
void MotorReg_Update(MotorReg_t * regulator);

double MotorReg_GetVelocity(MotorReg_t * regulator, double dt);
void MotorReg_SetVelocity(MotorReg_t * regulator, double velocity);



#endif /* INC_MOTOR_REG_H_ */
