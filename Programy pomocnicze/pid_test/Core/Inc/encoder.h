/*
 * encoder.h
 *
 *  Created on: Aug 11, 2021
 *      Author: marci
 */

#ifndef INC_ENCODER_H_
#define INC_ENCODER_H_

#include <stdbool.h>
#include <stdint.h>
#include "main.h"

typedef enum{
	encoder_software,
	encoder_hardware
}EncoderImplementation_t;

typedef struct{
	uint32_t pulsesPerRotation;
	uint32_t previousPosition;
	TIM_TypeDef * encoderTimer;
}EncoderHardware_t;

typedef struct{
	uint32_t pulsesPerRotation;
	int32_t previousPosition;
	uint32_t currentPosition;
	GPIO_TypeDef * encA_PinGpio;
	GPIO_TypeDef * encB_PinGpio;
	uint16_t encA_PinNumber;
	uint16_t encB_PinNumber;
	bool A_state;
	bool B_state;
}EncoderSoftware_t;

EncoderSoftware_t Encoder_InitSoftware(GPIO_TypeDef * EncA_PinGpio,
		GPIO_TypeDef * EncB_PinGpio, uint16_t EncA_PinNumber,
		uint16_t EncB_PinNumber, uint32_t pulsesPerRotation);

EncoderHardware_t Encoder_InitHardware(TIM_TypeDef * EncoderTimer, uint32_t pulsesPerRotation);

uint32_t Encoder_ReadSoftware(EncoderSoftware_t * encoder);
uint32_t Encoder_ReadHardware(EncoderHardware_t * encoder);
double Encoder_HardwareGetVelocity(EncoderHardware_t * encoder, double dt);
double Encoder_SoftwareGetVelocity(EncoderSoftware_t * encoder, double dt);

void Encoder_SoftwareCallback(EncoderSoftware_t * encoder, uint16_t gpioPin);

#endif /* INC_ENCODER_H_ */
