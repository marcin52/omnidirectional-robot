/*
 * encoder.c
 *
 *  Created on: Aug 11, 2021
 *      Author: marci
 */

/***************************
 * Includes
 *************************/

#include "encoder.h"

/***************************
 * Private functions prototypes
 *************************/

static inline bool getPinState(GPIO_TypeDef * gpio, uint8_t pinNumber);

/***************************
 * Public functions
 *************************/

EncoderSoftware_t Encoder_InitSoftware(GPIO_TypeDef * EncA_PinGpio,
		GPIO_TypeDef * EncB_PinGpio, uint16_t EncA_PinNumber,
		uint16_t EncB_PinNumber, uint32_t pulsesPerRotation){

	EncoderSoftware_t encoder;
	encoder.previousPosition = 0;
	encoder.currentPosition = 0;
	encoder.pulsesPerRotation = pulsesPerRotation;
	encoder.encA_PinGpio = EncA_PinGpio;
	encoder.encA_PinNumber = EncA_PinNumber;
	encoder.encB_PinGpio = EncB_PinGpio;
	encoder.encB_PinNumber = EncB_PinNumber;

	return encoder;
}

EncoderHardware_t Encoder_InitHardware(TIM_TypeDef * EncoderTimer, uint32_t pulsesPerRotation){

	EncoderHardware_t encoder;
	encoder.previousPosition = 0;
	encoder.pulsesPerRotation = pulsesPerRotation;
	encoder.encoderTimer = EncoderTimer;

	return encoder;

}

inline uint32_t Encoder_ReadSoftware(EncoderSoftware_t * encoder){
	return encoder->currentPosition;
}

inline uint32_t Encoder_ReadHardware(EncoderHardware_t * encoder){
	return encoder->encoderTimer->CNT;
}

double Encoder_HardwareGetVelocity(EncoderHardware_t * encoder, double dt){

	static uint32_t currentPos = 0;
	static double velocity = 0.0;

	currentPos = Encoder_ReadHardware(encoder);

	if(encoder->previousPosition > currentPos){
		velocity = ((double)(currentPos + UINT16_MAX) - (double)encoder->previousPosition) / dt;
	} else {
		velocity = ((double)currentPos - (double)encoder->previousPosition) / dt;
	}

	encoder->previousPosition = currentPos;
	return velocity / (double) encoder->pulsesPerRotation;
}

double Encoder_SoftwareGetVelocity(EncoderSoftware_t * encoder, double dt){

	static uint64_t currentPos = 0;
	static double velocity = 0.0;

	currentPos = Encoder_ReadSoftware(encoder);

	if(encoder->previousPosition > currentPos){
		velocity = ((double)(currentPos + UINT32_MAX) - (double)encoder->previousPosition) / dt;
	} else {
		velocity = ((double)currentPos - (double)encoder->previousPosition) / dt;
	}

	encoder->previousPosition = currentPos;
	return velocity / (double) encoder->pulsesPerRotation;
}

void Encoder_SoftwareCallback(EncoderSoftware_t * encoder, uint16_t gpioPin){

	encoder->A_state = getPinState(encoder->encA_PinGpio, encoder->encA_PinNumber);
	encoder->B_state = getPinState(encoder->encB_PinGpio, encoder->encB_PinNumber);

	if( gpioPin == encoder->encA_PinNumber || gpioPin == encoder->encB_PinNumber){

		if(encoder->A_state){
			if(encoder->B_state){
				encoder->currentPosition++;
			}else{
				encoder->currentPosition--;
			}
		}else{
			if(encoder->B_state){
				encoder->currentPosition--;
			}else{
				encoder->currentPosition++;
			}
		}
	}
}

/***************************
 * Private functions
 *************************/

static inline bool getPinState(GPIO_TypeDef * gpio, uint8_t pinNumber){

	return HAL_GPIO_ReadPin(gpio, pinNumber);

}

