/*
 * motor_config.h
 *
 *  Created on: Aug 14, 2021
 *      Author: marci
 */

#ifndef INC_MOTOR_CONFIG_H_
#define INC_MOTOR_CONFIG_H_

/***************************
 * Includes
 *************************/

#include "motor_reg.h"

/***************************
 * Public functions prototypes
 *************************/

MotorReg_t ConfigMotor1();
MotorReg_t ConfigMotor2();
MotorReg_t ConfigMotor3();

#endif /* INC_MOTOR_CONFIG_H_ */
