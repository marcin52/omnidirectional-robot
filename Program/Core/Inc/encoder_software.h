/*
 * encoder_software.h
 *
 *  Created on: Sep 24, 2021
 *      Author: marci
 */

#ifndef INC_ENCODER_SOFTWARE_H_
#define INC_ENCODER_SOFTWARE_H_

/***************************
 * Includes
 *************************/

#include "encoder.h"
#include "main.h"

/***************************
 * Class declaration
 *************************/

typedef struct{
	Encoder_t super; /* inherited Encoder */
	GPIO_TypeDef * encA_PinGpio;
	GPIO_TypeDef * encB_PinGpio;
	uint16_t encA_PinNumber;
	uint16_t encB_PinNumber;
	bool A_state;
	bool B_state;
}EncoderSoftware_t;

/***************************
 * Public functions prototypes
 *************************/

void EncoderSoftware_ctor(EncoderSoftware_t * me, GPIO_TypeDef * EncA_PinGpio,
		GPIO_TypeDef * EncB_PinGpio, uint16_t EncA_PinNumber,
		uint16_t EncB_PinNumber, uint32_t pulsesPerRotation);

void EncoderSoftware_Callback(EncoderSoftware_t * encoder, uint16_t gpioPin);

#endif /* INC_ENCODER_SOFTWARE_H_ */
