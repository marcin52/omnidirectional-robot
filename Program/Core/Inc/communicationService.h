/*
 * communicationService.h
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

#ifndef COMMUNICATION_COMMUNICATIONSERVICE_H_
#define COMMUNICATION_COMMUNICATIONSERVICE_H_

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "message_protocol.h"

#include <stdint.h>
#include <stdbool.h>

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

void CommService_Init();
void CommService_Task();
void CommService_PutToTheUartBuffer(uint8_t data);
void CommService_PutToTheOutputBuffer(message_TypeDef message);

/*******************************************************************************
 *    end of file
 ******************************************************************************/

#endif /* COMMUNICATION_COMMUNICATIONSERVICE_H_ */
