/*
 * encoder_hardware.h
 *
 *  Created on: Sep 24, 2021
 *      Author: marci
 */

#ifndef INC_ENCODER_HARDWARE_H_
#define INC_ENCODER_HARDWARE_H_

/***************************
 * Includes
 *************************/

#include "encoder.h"
#include "main.h"

/***************************
 * Class declaration
 *************************/

typedef struct{

	Encoder_t super; /* inherited Encoder */
	TIM_TypeDef * encoderTimer;

}EncoderHardware_t;

/***************************
 * Public functions prototypes
 *************************/

void EncoderHardware_ctor(EncoderHardware_t * me,
		TIM_TypeDef * EncoderTimer, uint32_t pulsesPerRotation);


#endif /* INC_ENCODER_HARDWARE_H_ */
