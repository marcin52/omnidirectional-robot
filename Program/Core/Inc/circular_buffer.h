//
// Created by marci on 2021-10-02.
//

#ifndef CSTD_BUFFER_H
#define CSTD_BUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>

#define CircularBuffer_define(type)                                                                                                                                                                                                       \
                                                                                                                                                                                                                                  \
      struct _buffer_##type;                                                                                                                                                                                                              \
                                                                                                                        \
      typedef struct                                                                                                                                                                                                       \
    {                                                                                                                                                                                                       \
        bool (*is_empty)(const struct _buffer_##type * const);                                                                                                                                                                                                       \
        bool (*is_full)(const struct _buffer_##type * const);                                                                                                                                                                                                        \
        int16_t (*write)(struct _buffer_##type * const, const type);                                                                                                                                                                                                       \
        int16_t (*overwrite)(struct _buffer_##type * const, const type);                                                                                                                                                                                                       \
        int16_t (*read)(struct _buffer_##type * const, type* const);                                                                                                                                                                                                      \
        void (*delete_buff)(struct _buffer_##type * const);                                                                                                                                                                                                              \
        void (*clear)(struct _buffer_##type * const);                                                                                                                                                                                     \
                                                                                                                                                \
    } _buffer_functions_##type;                                                                                                                                                                                                       \
                                                                                                                                                                                                          \
    typedef struct _buffer_##type                                                                                                                                                                                                       \
    {                                                                                                                                                                                                                             \
        _buffer_functions_##type const * _vtable;                                                                                                                                                                                                       \
        size_t capacity;                                                                                                                                                                                                       \
        size_t size;                                                                                                                                                                                                       \
        size_t head;                                                                                                                                                                                                              \
        size_t tail;                                                                                                                                                                                                              \
        type * data;                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
    } CircularBuffer_##type;                                                                                                                                                                                                       \
                                                                                                                                                                                                                                  \
    bool CircularBuffer_IsEmpty##type(const struct _buffer_##type * const buffer){                                                                                                                                                                                                       \
        return (buffer->size == 0);                                                                                                                                                                                                                                                   \
    }                                                                                                                                                                                                                                                                                 \
                                                                                                                                                                                                                                                                                           \
    bool CircularBuffer_IsFull##type(const struct _buffer_##type * const buffer){                                                                                                                                                                                                       \
        return ( buffer->size == buffer->capacity );                                                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                          \
    void CircularBuffer_Delete##type(struct _buffer_##type * const buffer ){                                                                                                                                                             \
        free(buffer->data);                                                                                                 \
        free(buffer);                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                      \
    int16_t CircularBuffer_Write##type(struct _buffer_##type * const buffer, const type val){                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        assert(buffer);                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        if( CircularBuffer_IsFull##type(buffer)){                                                                                                                                                                                                      \
            errno = ENOBUFS;                                                                                                                                                                                                      \
            return EXIT_FAILURE;                                                                                                                                                                                                      \
        }                                                                                                                                                                                                      \
        if(buffer->head >= buffer->capacity){                                                                                                                                                                                                      \
            buffer->head = 0;                                                                                                                                                                                                      \
        }                                                                                                                                                                                                      \
        buffer->data[buffer->head] = val;                                                                                                                                                                                                      \
        buffer->head += 1;                                                                                                                                                                                                      \
        buffer->size += 1;                                                                                                                                                                                                      \
        return EXIT_SUCCESS;                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                      \
    int16_t CircularBuffer_Overwrite##type( struct _buffer_##type * const buffer, const type val){                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        assert(buffer);                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        if( !CircularBuffer_IsFull##type(buffer)){                                                                                                                                                                                                      \
            return CircularBuffer_Write##type(buffer, val);                                                                                                                                                                                                      \
        } else {                                                                                                                                                                                                      \
            if(buffer->head >= buffer->capacity){                                                                                                                                                                                                      \
                buffer->head = 0;                                                                                                                                                                                                      \
            }                                                                                                                                                                                                      \
            buffer->data[buffer->head] = val;                                                                                                                                                                                                      \
            buffer->head += 1;                                                                                                                                                                                                      \
            buffer->tail += 1;                                                                                                                                                                                                      \
            return EXIT_SUCCESS;                                                                                                                                                                                                      \
        }                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                      \
    int16_t CircularBuffer_Read##type( struct _buffer_##type * const buffer, type * const val){                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        assert(buffer);                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        if( CircularBuffer_IsEmpty##type(buffer)){                                                                                                                                                                                                      \
            errno = ENODATA;                                                                                                                                                                                                      \
            return EXIT_FAILURE;                                                                                                                                                                                                      \
        }                                                                                                                                                                                                      \
        if(buffer->tail >= buffer->capacity){                                                                                                                                                                                                      \
            buffer->tail = 0;                                                                                                                                                                                                      \
        }                                                                                                                                                                                                      \
        *val = buffer->data[buffer->tail];                                                                                                                                                                                                      \
        buffer->tail += 1;                                                                                                                                                                                                      \
        buffer->size -= 1;                                                                                                                                                                                                      \
        return EXIT_SUCCESS;                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                      \
    void CircularBuffer_Clear##type( struct _buffer_##type * const buffer){                                                                                                                                                                                                      \
        assert(buffer);                                                                                                                                                                                                      \
        buffer->head = 0;                                                                                                                                                                                                      \
        buffer->tail = 0;                                                                                                                                                                                                      \
        buffer->size = 0;                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                                                          \
    void CircularBuffer_Ctor##type( struct _buffer_##type * const buffer, const int capacity, type * data_buffer){                                                                                                                                                                                                       \
                                                                                                                                                                                                                                          \
        static const _buffer_functions_##type vtable = {                                                                                                                                                                                                      \
                &CircularBuffer_IsEmpty##type,                                                                                                                                                                                                      \
                &CircularBuffer_IsFull##type,                                                                                                                                                                                                                         \
                &CircularBuffer_Write##type,                                                                                                                                                                                                      \
                &CircularBuffer_Overwrite##type,                                                                                                                                                                                                                         \
                &CircularBuffer_Read##type,                                                                                                                                                                                                      \
                &CircularBuffer_Delete##type,                                                                                                                                                                                                                         \
                &CircularBuffer_Clear##type                                                                                                                                                                                                                  \
                };                                                                                                                                                                                                      \
                                                                                                                                                                                                                                          \
        buffer->_vtable = &vtable;                                                                                                                                                                                                                          \
        buffer->data = data_buffer;                                                                                                                                                                                                      \
        buffer->capacity = capacity;                                                                                                                                                                                                      \
        buffer->head = 0;                                                                                                                                                                                                      \
        buffer->tail = 0;                                                                                                                                                                                                      \
        buffer->size = 0;                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                             \
                                                                                                                                                                                                                                          \
    CircularBuffer_##type * CircularBuffer_New##type(const int capacity){                                                                                                                                                                                                      \
                                                                                                                                                                                                          \
        if(capacity < 1){                                                                                                                                                                                                      \
            return NULL;                                                                                                                                                                                                      \
    }                                                                                                                                                                                                      \
                                                                                                                                                                                                      \
        CircularBuffer_##type  *b = malloc(sizeof(CircularBuffer_##type ));                                                                                                                                                                                                      \
            void * data = malloc(capacity * sizeof(type));                                                                                                                                                                                                      \
                                                                                                                                                                                                                  \
            if(b){                                                                                                                                                                                                      \
            CircularBuffer_Ctor##type( b, capacity, data);                                                                                                                                                                                                      \
        }                                                                                                                                                                                                      \
                                                                                                                                                                                                                                                                          \
        return b;                                                                                                                                                                                                      \
    }

#define CircularBuffer(type)                                                                                                                                                                                                       \
    CircularBuffer_##type

#define CircularBuffer_New(type, capacity)                                                                                                                                                                                                       \
    CircularBuffer_New##type(capacity)

#define CircularBuffer_Ctor(type, b, capacity, data)                                                                                                                                                                                                       \
    CircularBuffer_Ctor##type(b, capacity, data)

#define CircularBuffer_IsFull(buffer)                                                                                                                                                                                                       \
    (buffer)->_vtable->is_full(buffer)

#define CircularBuffer_IsEmpty(buffer)                                                                                                                                                                                                       \
    (buffer)->_vtable->is_empty(buffer)

#define CircularBuffer_Write(buffer, val)                                                                                                                                                                                                       \
    (buffer)->_vtable->write(buffer, val)

#define CircularBuffer_Overwrite(buffer, val)                                                                                                                                                                                                       \
    (buffer)->_vtable->overwrite(buffer, val)

#define CircularBuffer_Read(buffer, val)                                                                                                                                                                                                       \
    (buffer)->_vtable->read(buffer, val)

#define CircularBuffer_Clear(buffer)                                                                                                                                                                                                       \
    (buffer)->_vtable->clear(buffer)

#define CircularBuffer_Delete(buffer)                                                                                                                                                                                                       \
    (buffer)->_vtable->delete_buff(buffer)

#ifdef __cplusplus
}
#endif

#endif //CSTD_BUFFER_H

/***********************************************
* End of File
***********************************************/
