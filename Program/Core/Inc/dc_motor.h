/*
 * dc_motor.h
 *
 *  Created on: 13 sie 2021
 *      Author: marci
 */

#ifndef INC_DC_MOTOR_H_
#define INC_DC_MOTOR_H_

/***************************
 * Includes
 *************************/

#include <stdint.h>
#include "main.h"

/***************************
 * Typedefs
 *************************/

typedef enum{
	MotorDirection_foreward,
	MotorDirection_backward
}MotorDirections_t;

typedef struct motor_dc{
	TIM_TypeDef * timer;
	uint16_t timChannel;
	GPIO_TypeDef * pinGpioA;
	uint16_t pinNumberA;
	GPIO_TypeDef * pinGpioB;
	uint8_t pinNumberB;
}DcMotor_t;

/***************************
 * Public functions prototypes
 *************************/

DcMotor_t DcMotor_Create(TIM_TypeDef * timer, uint8_t timChannel, GPIO_TypeDef * pinGpioA,
					uint16_t pinNumberA, GPIO_TypeDef * pinGpioB, uint16_t pinNumberB);
void DcMotor_Write(DcMotor_t * motor, int16_t value);

#endif /* INC_DC_MOTOR_H_ */
