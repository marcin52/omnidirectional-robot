/*
 * motor_reg.h
 *
 *  Created on: Jun 15, 2021
 *      Author: marci
 */

#ifndef INC_MOTOR_REG_H_
#define INC_MOTOR_REG_H_

/***************************
 * Includes
 *************************/

#include "main.h"
#include <stdbool.h>

#include "encoder.h"
#include "dc_motor.h"

/***************************
 * Typedefs
 *************************/

typedef struct{
	double Kp;
	double Ki;
	double Kd;
	double dt;
	double error;
	double errorInt;
	double setVelocity;
	double encVelocity;
	double maxIntVal;
	double derivative;
	double pre_error;
	int32_t output;
}PidReg_t;

typedef struct{
	Encoder_t * encoder;
	DcMotor_t * motor;
	PidReg_t * pid;
}MotorReg_t;

/***************************
 * Public functions prototypes
 *************************/

MotorReg_t MotorReg_Create(PidReg_t * pidReg, DcMotor_t * motor,
		Encoder_t * encoder);

PidReg_t MotorReg_CreatePid(double dt, double maxInt,
		double Kp, double Ki, double Kd);

void MotorReg_Update(MotorReg_t * regulator);
double MotorReg_GetVelocity(MotorReg_t * regulator, double dt);
void MotorReg_SetVelocity(MotorReg_t * regulator, double velocity);



#endif /* INC_MOTOR_REG_H_ */
