/*
 * omnidirectional_control.h
 *
 *  Created on: Aug 14, 2021
 *      Author: marci
 */

#ifndef INC_OMNIDIRECTIONAL_CONTROL_H_
#define INC_OMNIDIRECTIONAL_CONTROL_H_

/***************************
 * Includes
 *************************/

#include <stdbool.h>
#include "motor_reg.h"

/***************************
 * Typedefs
 *************************/

typedef struct{
	double w;
	double v_x;
	double v_y;
}DriveVectorBodyFrame_t;

typedef struct{
	double u1;
	double u2;
	double u3;
}DriveVectorMotorsFrame_t;

/***************************
 * Public functions prototypes
 *************************/

void OmniControl_Init(MotorReg_t * restrict motor1,
		MotorReg_t * restrict motor2, MotorReg_t * restrict motor3);
void OmniControl_RobotStart();
void OmniControl_RobotStop();
void OmniControl_SetDriveVector(DriveVectorBodyFrame_t * vector);
void OmniControl_Callback();

#endif /* INC_OMNIDIRECTIONAL_CONTROL_H_ */
