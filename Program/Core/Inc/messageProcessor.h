/*
 * messageProcessor.h
 *
 *  Created on: 6 lis 2020
 *      Author: marci
 */

#ifndef COMMUNICATION_MESSAGEPROCESSOR_H_
#define COMMUNICATION_MESSAGEPROCESSOR_H_

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "message_protocol.h"

#include <stdint.h>
#include <stdbool.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

// Messages IDs definitions

#define ROBOT_ON_OFF_ID				11
#define VELOCITY_VECTOR_ID			12

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

void MessageProcessor_processMessage(message_TypeDef message);

/*******************************************************************************
 *    end of file
 ******************************************************************************/

#endif /* COMMUNICATION_MESSAGEPROCESSOR_H_ */
