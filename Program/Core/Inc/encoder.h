/*
 * encoder.h
 *
 *  Created on: Aug 11, 2021
 *      Author: marci
 */

#ifndef INC_ENCODER_H_
#define INC_ENCODER_H_

/***************************
 * Includes
 *************************/

#include <stdbool.h>
#include <stdint.h>

/***************************
 * Class declaration
 *************************/

typedef struct {
    struct EncoderVtable const *vptr; /* virtual pointer */

	uint32_t pulsesPerRotation;
	uint32_t previousPosition;
	uint32_t currentPosition;
} Encoder_t;

struct EncoderVtable {
    uint32_t (*read)(Encoder_t const * const me);
    double (*getVelocity)(Encoder_t * const me, double dt);
};

/***************************
 * Virtual methods
 *************************/

uint32_t Encoder_Read(Encoder_t const * const me);
double Encoder_GetVelocity(Encoder_t * const me, double dt);

/***************************
 * Public functions prototypes
 *************************/

void Encoder_ctor(Encoder_t * me,  uint32_t pulsesPerRotation);
uint32_t Encoder_BaseRead(Encoder_t const * const me);
double Encoder_BaseGetVelocity(Encoder_t * const me, double dt);

#endif /* INC_ENCODER_H_ */
