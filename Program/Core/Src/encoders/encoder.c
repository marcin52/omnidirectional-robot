/*
 * encoder.c
 *
 *  Created on: Aug 11, 2021
 *      Author: marci
 */

/***************************
 * Includes
 *************************/

#include "encoder.h"

/***************************
 * Private functions prototypes
 *************************/

static inline int32_t getDifference(uint32_t curr, uint32_t prev);

/***************************
 * Virtual methods
 *************************/

uint32_t Encoder_Read(Encoder_t const * const me){
	 return (*me->vptr->read)(me);
}

double Encoder_GetVelocity(Encoder_t * const me, double dt){
	return (*me->vptr->getVelocity)(me, dt);
}

/***************************
 * Public functions
 *************************/

void Encoder_ctor(Encoder_t * me,  uint32_t pulsesPerRotation){

    static const struct EncoderVtable vtable = {
        &Encoder_BaseRead,
        &Encoder_BaseGetVelocity
    };
    me->vptr = &vtable;

	me->pulsesPerRotation = pulsesPerRotation;
	me->previousPosition = 0;
	me->currentPosition = 0;;

}

uint32_t Encoder_BaseRead(Encoder_t const * const me) {
   (void)me; /* unused parameter */
	return 0U;
}

double Encoder_BaseGetVelocity(Encoder_t * const me, double dt) {

	me->currentPosition = Encoder_Read(me);

	int32_t difference = getDifference(me->currentPosition, me->previousPosition);

	me->previousPosition = 	me->currentPosition;

	return (difference / dt) / (double) me->pulsesPerRotation;
}

/***************************
 * Private functions
 *************************/

static inline int32_t getDifference(uint32_t curr, uint32_t prev){

	int16_t difference = 0;

	difference = (curr - prev);

	return difference;
}
