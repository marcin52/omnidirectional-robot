/*
 * encoder_hardware.c
 *
 *  Created on: Sep 24, 2021
 *      Author: marci
 */

/***************************
 * Includes
 *************************/

#include "encoder_hardware.h"

/***************************
 * Private functions prototypes
 *************************/

static inline uint32_t Encoder_ReadHardware(EncoderHardware_t * encoder);

/***************************
 * Public functions
 *************************/

void EncoderHardware_ctor(EncoderHardware_t * me,
		TIM_TypeDef * EncoderTimer, uint32_t pulsesPerRotation){

    static const struct EncoderVtable vtable = {
        (uint32_t (*)(Encoder_t const * const me))&Encoder_ReadHardware,
        (double (*)(Encoder_t * const me, double dt))&Encoder_BaseGetVelocity
    };

    Encoder_ctor(&me->super, pulsesPerRotation);
    me->super.vptr = &vtable;

    me->encoderTimer = EncoderTimer;
}

/***************************
 * Private functions
 *************************/

static inline uint32_t Encoder_ReadHardware(EncoderHardware_t * encoder){
	return encoder->encoderTimer->CNT;
}

