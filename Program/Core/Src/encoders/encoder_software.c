/*
 * encoder_software.c
 *
 *  Created on: Sep 24, 2021
 *      Author: marci
 */


/***************************
 * Includes
 *************************/

#include "encoder_software.h"

/***************************
 * Private functions prototypes
 *************************/

static inline bool getPinState(GPIO_TypeDef * gpio, uint8_t pinNumber);
static inline uint32_t encoderReadSoftware(EncoderSoftware_t * me);

/***************************
 * Public functions
 *************************/

void EncoderSoftware_ctor(EncoderSoftware_t * me, GPIO_TypeDef * EncA_PinGpio,
		GPIO_TypeDef * EncB_PinGpio, uint16_t EncA_PinNumber,
		uint16_t EncB_PinNumber, uint32_t pulsesPerRotation){

    static const struct EncoderVtable vtable = {
        (uint32_t (*)(Encoder_t const * const me))&encoderReadSoftware,
        (double (*)(Encoder_t * const me, double dt))&Encoder_BaseGetVelocity
    };

    Encoder_ctor(&me->super, pulsesPerRotation);
    me->super.vptr = &vtable;

	me->encA_PinGpio = EncA_PinGpio;
	me->encA_PinNumber = EncA_PinNumber;
	me->encB_PinGpio = EncB_PinGpio;
	me->encB_PinNumber = EncB_PinNumber;

}

void EncoderSoftware_Callback(EncoderSoftware_t * encoder, uint16_t gpioPin){

	encoder->A_state = HAL_GPIO_ReadPin(encoder->encA_PinGpio, encoder->encA_PinNumber);
	encoder->B_state = HAL_GPIO_ReadPin(encoder->encB_PinGpio, encoder->encB_PinNumber);

	if( gpioPin == encoder->encA_PinNumber ){

		if(encoder->A_state){
			if(encoder->B_state){
				encoder->super.currentPosition++;
			}else{
				encoder->super.currentPosition--;
			}
		}else{
			if(encoder->B_state){
				encoder->super.currentPosition--;
			}else{
				encoder->super.currentPosition++;
			}
		}
	} else if( gpioPin == encoder->encB_PinNumber){

		if(encoder->B_state){
			if(encoder->A_state){
				encoder->super.currentPosition--;
			}else{
				encoder->super.currentPosition++;
			}
		}else{
			if(encoder->A_state){
				encoder->super.currentPosition++;
			}else{
				encoder->super.currentPosition--;
			}
		}
	}
}

/***************************
 * Private functions
 *************************/

static inline bool getPinState(GPIO_TypeDef * gpio, uint8_t pinNumber){

	return HAL_GPIO_ReadPin(gpio, pinNumber);

}

static inline uint32_t encoderReadSoftware(EncoderSoftware_t * me){
	return me->super.currentPosition;
}
