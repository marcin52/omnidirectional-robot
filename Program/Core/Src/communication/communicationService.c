/*
 * communicationService.c
 *
 *  Created on: 5 lis 2020
 *      Author: marci
 */

#include "communicationService.h"

#include "usart.h"
#include "messageProcessor.h"
#include "circular_buffer.h"
#include "message_protocol.h"

#include <stdlib.h>

/*******************************************************************************
 *    Defines
 ******************************************************************************/

CircularBuffer_define(uint8_t)
CircularBuffer_define(message_TypeDef)

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

typedef enum{
	MSG_NOT_STARTED,
	MSG_STARTED,
	MSG_STARTED_AFTER_ID,
	MSG_TO_END
}msg_state_t;

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/* UART buffer */

static CircularBuffer(uint8_t) uart_in_buffer;
static uint8_t uart_in_buffer_data[BUFFER_SIZE];

static msg_state_t receivedMsgState = MSG_NOT_STARTED;
static message_TypeDef receivedMessage = {0};
static uint8_t messageReceivedDataCounter = 0;

/* MESSAGES buffers */

static CircularBuffer(message_TypeDef) input_msg_buffer;
static message_TypeDef input_msg_buffer_Data[MESSAGE_BUFFER_SIZE];

static CircularBuffer(message_TypeDef) output_msg_buffer;
static message_TypeDef output_msg_buffer_data[MESSAGE_BUFFER_SIZE];

/***************************
* Private functions prototypes
***************************/

static inline uint8_t convertASCIItoINT(uint8_t character);
static inline uint8_t convertINTtoASCII(uint8_t uintValue);

static inline bool checkIfFirstCharacterOK(uint8_t character);
static inline bool checkIfLastCharacterOK(uint8_t character);

static void sendOutputMsg();
static void handleInputMsg();

static inline void handleMsgNotStartedState(uint8_t character);
static inline void handleMsgStartedState(uint8_t character);
static void handleMsgStartedAfterIDState(uint8_t character);
static void handleMessageToEndState(uint8_t character);
static void handleReceivedChar(uint8_t character);

static void composeInputMsgs();
static void composeOutputMsg(uint8_t * outputData);

/***************************
* Public functions
***************************/

void CommService_Init(){

	UART_ComInit();

	CircularBuffer_Ctor(uint8_t, &uart_in_buffer, BUFFER_SIZE, uart_in_buffer_data);

	CircularBuffer_Ctor(message_TypeDef, &input_msg_buffer,
			MESSAGE_BUFFER_SIZE, input_msg_buffer_Data);

	CircularBuffer_Ctor(message_TypeDef, &output_msg_buffer,
			MESSAGE_BUFFER_SIZE, output_msg_buffer_data);
}

void CommService_Task(){

	composeInputMsgs();
	handleInputMsg();
	sendOutputMsg();
}

void CommService_PutToTheUartBuffer(uint8_t data){

	CircularBuffer_Write(&uart_in_buffer, data);
}

void CommService_PutToTheOutputBuffer(message_TypeDef message){

	CircularBuffer_Write(&output_msg_buffer, message);
}

/***************************
* Private functions
***************************/

/*
 * helper functions
 */
static inline uint8_t convertASCIItoINT(uint8_t character){
	return character - 48;
}

static inline uint8_t convertINTtoASCII(uint8_t uintValue){
	return uintValue + 48;
}

static inline bool checkIfFirstCharacterOK(uint8_t character){
	return (MESSAGE_START_CHAR == character);
}

static inline bool checkIfLastCharacterOK(uint8_t character){
	return (MESSAGE_END_CHAR == character);
}

static void sendOutputMsg(){

	if(CircularBuffer_IsEmpty(&output_msg_buffer)){
		return;
	}

	uint8_t dataToSend[TX_BUFFER_SIZE];
	composeOutputMsg(dataToSend);
	UART_SendData(dataToSend, TX_BUFFER_SIZE);

}

static void handleInputMsg(){

	if(CircularBuffer_IsEmpty(&input_msg_buffer)){
		return;
	}

	message_TypeDef inputMsg = {0};
	CircularBuffer_Read(&input_msg_buffer, &inputMsg);

	MessageProcessor_processMessage(inputMsg);
}

/* Characters handling */

static inline void handleMsgNotStartedState(uint8_t character){

	if( checkIfFirstCharacterOK(character) ){
		receivedMsgState = MSG_STARTED;
	}
}

static inline void handleMsgStartedState(uint8_t character){

	receivedMessage.ID = character;
	receivedMsgState = MSG_STARTED_AFTER_ID;

}

static void handleMsgStartedAfterIDState(uint8_t character){

	receivedMessage.message[messageReceivedDataCounter] = character;
	messageReceivedDataCounter++;

	if( messageReceivedDataCounter == MESSAGE_MAX_MESSAGE_SIZE){
		receivedMsgState = MSG_TO_END;
		messageReceivedDataCounter = 0;
	}
}

static void handleMessageToEndState(uint8_t character){

	if( checkIfLastCharacterOK(character) ){
		CircularBuffer_Write(&input_msg_buffer, receivedMessage);
	} else{
		receivedMessage.ID = 0;
		receivedMessage.message[0] = 0;
		receivedMessage.message[1] = 0;
		receivedMessage.message[2] = 0;
	}
	receivedMsgState = MSG_NOT_STARTED;
}

static void handleReceivedChar(uint8_t character){

	switch(receivedMsgState){
		case MSG_NOT_STARTED:
			handleMsgNotStartedState(character);
		break;
		case MSG_STARTED:
			handleMsgStartedState(character);
		break;
		case MSG_STARTED_AFTER_ID:
			handleMsgStartedAfterIDState(character);
		break;
		case MSG_TO_END:
			handleMessageToEndState(character);
		break;
	}
}

/* Messages composing */

static void composeInputMsgs(){

	uint8_t val;

	if( CircularBuffer_Read(&uart_in_buffer, &val) == EXIT_SUCCESS){

		handleReceivedChar(val);
	}
}

static void composeOutputMsg(uint8_t * outputData){

	message_TypeDef messageToSend = {0};

	CircularBuffer_Read(&output_msg_buffer, &messageToSend);

	outputData[MESSAGE_START_CHAR_POSITION] = MESSAGE_START_CHAR;
	outputData[MESSAGE_ID_CHAR_POSITION] = convertINTtoASCII(messageToSend.ID);
	outputData[MESSAGE_STOP_CHAR_POSITION] = MESSAGE_END_CHAR;

	for( uint8_t i = 0; i < MESSAGE_MAX_MESSAGE_SIZE; i++){

		outputData[i + MESSAGE_VALUE_CHARS_POSITION] \
						= messageToSend.message[i];
	}
}
