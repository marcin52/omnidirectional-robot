/*
 * messageProcessor.c
 *
 *  Created on: 6 lis 2020
 *      Author: marci
 */

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "messageProcessor.h"

#include "communicationService.h"
#include "omnidirectional_control.h"

/*******************************************************************************
 *    PRIVATE VARIABLES
 ******************************************************************************/

static DriveVectorBodyFrame_t vector;

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

static void messageIdNotRecognizedError();
static void handleRobotOnOffMessage(uint8_t * msg);
static void handleVelocityMessage(uint8_t * msg);
static void messageReceivingStateMachine(message_TypeDef message);

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void MessageProcessor_processMessage(message_TypeDef message){

	messageReceivingStateMachine( message );
}

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static void messageIdNotRecognizedError(){
	// TODO
}

static void handleRobotOnOffMessage(uint8_t * msg){

	if( 1 == msg[0] ){
		OmniControl_RobotStart();
	} else {
		OmniControl_RobotStop();
	}
}

static void handleVelocityMessage(uint8_t * msg){

	vector.w = (msg[0] - 64)*16.40625;
	vector.v_x = (int8_t)msg[1];
	vector.v_y = (int8_t)msg[2];

	OmniControl_SetDriveVector(&vector);
}

static void messageReceivingStateMachine(message_TypeDef message){

	switch(message.ID){
		case ROBOT_ON_OFF_ID:
			handleRobotOnOffMessage(message.message);
		break;
		case VELOCITY_VECTOR_ID:
			handleVelocityMessage(message.message);
		break;
		default:
			messageIdNotRecognizedError();
		break;
	}
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/


