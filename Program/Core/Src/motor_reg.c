/*
 * motor_reg.c
 *
 *  Created on: Jun 15, 2021
 *      Author: marci
 */

#include "motor_reg.h"

/***************************
 * Private functions prototypes
 *************************/

void motorStop(MotorReg_t * regulator);

/***************************
 * Public functions
 *************************/

PidReg_t MotorReg_CreatePid(double dt, double maxInt, double Kp, double Ki, double Kd){

	PidReg_t reg;

	reg.errorInt = 0.0;
	reg.pre_error = 0.0;
	reg.dt = dt;
	reg.maxIntVal = maxInt;

	reg.Kp = Kp;
	reg.Kd = Kd;
	reg.Ki = Ki;

	return reg;
}

MotorReg_t MotorReg_Create(PidReg_t * pidReg, DcMotor_t * motor,
		Encoder_t * encoder){

	MotorReg_t reg;
	reg.pid = pidReg;
	reg.motor = motor;
	reg.encoder = encoder;

	return reg;
}

void MotorReg_Update(MotorReg_t * reg){

	if(reg->pid->setVelocity == 0.0){
		motorStop(reg);
	}

	static int16_t output = 0.0;

	reg->pid->encVelocity = MotorReg_GetVelocity(reg, reg->pid->dt);

	reg->pid->error = reg->pid->setVelocity - reg->pid->encVelocity;

	output = reg->pid->error * reg->pid->Kp;

	reg->pid->errorInt += reg->pid->error * reg->pid->dt;

	if( reg->pid->errorInt > reg->pid->maxIntVal ){
		reg->pid->errorInt = reg->pid->maxIntVal;
	}

	if( reg->pid->errorInt < -reg->pid->maxIntVal ){
		reg->pid->errorInt = -reg->pid->maxIntVal;
	}

	output += reg->pid->errorInt * reg->pid->Ki;

	reg->pid->derivative = (reg->pid->error - reg->pid->pre_error) / reg->pid->dt;
	output += reg->pid->derivative * reg->pid->Kd;

	if(output > 1000){
		output = 1000;
	}

	if(output < -1000){
		output = -1000;
	}

	reg->pid->output = output;

	DcMotor_Write(reg->motor, output);

	reg->pid->pre_error = reg->pid->error;

}

double MotorReg_GetVelocity(MotorReg_t * regulator, double dt){

	return Encoder_GetVelocity(regulator->encoder, dt);

}

inline void MotorReg_SetVelocity(MotorReg_t * regulator, double velocity){

	regulator->pid->setVelocity = velocity;

}


/***************************
 * Private functions
 *************************/

void motorStop(MotorReg_t * regulator){
	DcMotor_Write(regulator->motor, 0);
}
