/*
 * dc_motor.c
 *
 *  Created on: 13 sie 2021
 *      Author: marci
 */

#include "dc_motor.h"

/***************************
 * Private functions prototypes
 *************************/

static void writeToMotorPWM(DcMotor_t * motor, uint32_t value);
static void writeToMotorDirectionPins(DcMotor_t * motor, MotorDirections_t direction);
static void writeToMotorGpios(DcMotor_t * motor, uint8_t pinA_value, uint8_t pinB_value);

/***************************
 * Public functions
 *************************/

DcMotor_t DcMotor_Create(TIM_TypeDef * timer, uint8_t timChannel, GPIO_TypeDef * pinGpioA,
					uint16_t pinNumberA, GPIO_TypeDef * pinGpioB, uint16_t pinNumberB){

	DcMotor_t motor;
	motor.timer = timer;
	motor.timChannel = timChannel;
	motor.pinGpioA = pinGpioA;
	motor.pinNumberA = pinNumberA;
	motor.pinGpioB = pinGpioB;
	motor.pinNumberB = pinNumberB;

	return motor;

}

void DcMotor_Write(DcMotor_t * motor, int16_t value){

	MotorDirections_t direction;

	if( value < 0){
		direction = MotorDirection_backward;
		value = - value;
	} else {
		direction = MotorDirection_foreward;
	}

	writeToMotorDirectionPins(motor, direction);
	writeToMotorPWM(motor, value);

}

/***************************
 * Private functions
 *************************/


static void writeToMotorPWM(DcMotor_t * motor, uint32_t value){

	switch(motor->timChannel){
		case 1:
			motor->timer->CCR1 = value;
		break;
		case 2:
			motor->timer->CCR2 = value;
		break;
		case 3:
			motor->timer->CCR3 = value;
		break;
		case 4:
			motor->timer->CCR4 = value;
		break;
		case 5:
			motor->timer->CCR5 = value;
		break;
		case 6:
			motor->timer->CCR6 = value;
		break;
		default:
		break;
	}
}

static void writeToMotorDirectionPins(DcMotor_t * motor, MotorDirections_t direction){

	switch(direction){

		case MotorDirection_foreward:
			writeToMotorGpios(motor, 0, 1);
		break;
		case MotorDirection_backward:
			writeToMotorGpios(motor, 1, 0);
		break;
		default:
		break;
	}
}

static inline void writeToMotorGpios(DcMotor_t * motor, uint8_t pinA_value, uint8_t pinB_value){

	HAL_GPIO_WritePin(motor->pinGpioA, motor->pinNumberA, pinA_value);
	HAL_GPIO_WritePin(motor->pinGpioB, motor->pinNumberB, pinB_value);

}
