/*
 * omnidirectional_control.c
 *
 *  Created on: Aug 14, 2021
 *      Author: marci
 */

/***************************
 * Includes
 *************************/

#include "omnidirectional_control.h"
#include <math.h>

#define MAX_SPEED			0.40
#define MAX_INPUT_VECTOR	4200.0

/***************************
 * Private functions prototypes
 *************************/

static void calculateMotorVector(DriveVectorBodyFrame_t * d_vector);
static inline void scaleMotorVector(DriveVectorMotorsFrame_t * d_vector);
static inline double applyScale(double var);

/***************************
 * Private variables
 *************************/

static const double R = 0.03; // Wheels radius
static const double D = 0.12; // Distance from wheels to center of robot
static double sinPi_3 = 0.0;

static MotorReg_t * firstMotor;
static MotorReg_t * secondMotor;
static MotorReg_t * thirdMotor;

static DriveVectorMotorsFrame_t motorVec = {0};

static bool isRobotStarted = false;

/***************************
 * Public functions
 *************************/

void OmniControl_Init(MotorReg_t * motor1, MotorReg_t * motor2, MotorReg_t * motor3){

	sinPi_3 = sin(M_PI / (double) 3);

	firstMotor = motor1;
	secondMotor = motor2;
	thirdMotor = motor3;

	isRobotStarted = false;

	MotorReg_SetVelocity(firstMotor, 0.0);
	MotorReg_SetVelocity(secondMotor, 0.0);
	MotorReg_SetVelocity(thirdMotor, 0.0);
}

inline void OmniControl_RobotStart(){

	isRobotStarted = true;

}

inline void OmniControl_RobotStop(){

	isRobotStarted = false;
}

void OmniControl_SetDriveVector(DriveVectorBodyFrame_t * vector){

	if(isRobotStarted){

		calculateMotorVector(vector);
		scaleMotorVector(&motorVec);

		MotorReg_SetVelocity(firstMotor, motorVec.u1);
		MotorReg_SetVelocity(secondMotor, motorVec.u2);
		MotorReg_SetVelocity(thirdMotor, motorVec.u3);

	} else {
		MotorReg_SetVelocity(firstMotor, 0.0);
		MotorReg_SetVelocity(secondMotor, 0.0);
		MotorReg_SetVelocity(thirdMotor, 0.0);
	}
}

inline void OmniControl_Callback(){
	MotorReg_Update(firstMotor);
	MotorReg_Update(secondMotor);
	MotorReg_Update(thirdMotor);
}

/***************************
 * Private functions
 *************************/

static void calculateMotorVector(DriveVectorBodyFrame_t * d_vector){

	motorVec.u1 = -D/R * d_vector->w + 1.0/R * d_vector->v_x;
	motorVec.u2 = -D/R * d_vector->w - 1.0/(2*R) * d_vector->v_x - sinPi_3 / R * d_vector->v_y;
	motorVec.u3 = -D/R * d_vector->w - 1.0/(2*R) * d_vector->v_x + sinPi_3 / R * d_vector->v_y;
}

static inline void scaleMotorVector(DriveVectorMotorsFrame_t * d_vector){

	d_vector->u1 = applyScale(d_vector->u1);
	d_vector->u2 = applyScale(d_vector->u2);
	d_vector->u3 = applyScale(d_vector->u3);
}

static inline double applyScale(double var){

	return var * MAX_SPEED / MAX_INPUT_VECTOR;

}
