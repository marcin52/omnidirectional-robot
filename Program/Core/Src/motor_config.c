/*
 * motor_config.c
 *
 *  Created on: Aug 14, 2021
 *      Author: marci
 */

#include "main.h"
#include "motor_config.h"

#include "encoder_hardware.h"
#include "encoder_software.h"

#define ENCODER_PULSES_PER_ROTATION					 	11
#define ENCODER_DETECTED_EDGES							4
#define MOTOR_TRANSMISSION								171

static const uint32_t encoderPulsesPerRotation = MOTOR_TRANSMISSION * ENCODER_DETECTED_EDGES * ENCODER_PULSES_PER_ROTATION;

static const double delta_t = 0.05;
static const double maxPidIntegralError = 100000;

static const double Kp1 = 5000;
static const double Ki1 = 20000;
static const double Kd1 = 10;

static const double Kp2 = 5000;
static const double Ki2 = 20000;
static const double Kd2 = 10;

static const double Kp3 = 5000;
static const double Ki3 = 20000;
static const double Kd3 = 10;

EncoderHardware_t encoder1;
DcMotor_t motor1;
PidReg_t pid1;
EncoderHardware_t encoder2;
DcMotor_t motor2;
PidReg_t pid2;
EncoderSoftware_t encoder3;
DcMotor_t motor3;
PidReg_t pid3;

MotorReg_t ConfigMotor1(){

	EncoderHardware_ctor(&encoder1, TIM1, encoderPulsesPerRotation);
	motor1 = DcMotor_Create(TIM15, 1, MOTOR1_A_GPIO_Port, MOTOR1_A_Pin, MOTOR1_B_GPIO_Port, MOTOR1_B_Pin);
	pid1 = MotorReg_CreatePid(delta_t, maxPidIntegralError, Kp1, Ki1, Kd1);

	return MotorReg_Create(&pid1, &motor1, (Encoder_t *)&encoder1);
}

MotorReg_t ConfigMotor2(){

	EncoderHardware_ctor(&encoder2, TIM2, encoderPulsesPerRotation);
	motor2 = DcMotor_Create(TIM16, 1, MOTOR3_A_GPIO_Port, MOTOR3_A_Pin, MOTOR3_B_GPIO_Port, MOTOR3_B_Pin);
	pid2 = MotorReg_CreatePid(delta_t, maxPidIntegralError, Kp2, Ki2, Kd2);

	return MotorReg_Create(&pid2, &motor2, (Encoder_t *)&encoder2);
}

MotorReg_t ConfigMotor3(){

	EncoderSoftware_ctor(&encoder3, ENC3_A_GPIO_Port, ENC3_B_GPIO_Port, ENC3_A_Pin,
									ENC3_B_Pin, encoderPulsesPerRotation);
	motor3 = DcMotor_Create(TIM15, 2, MOTOR2_B_GPIO_Port, MOTOR2_B_Pin, MOTOR2_A_GPIO_Port, MOTOR2_A_Pin);
	pid3 = MotorReg_CreatePid(delta_t, maxPidIntegralError, Kp3, Ki3, Kd3);

	return MotorReg_Create(&pid3, &motor3, (Encoder_t *)&encoder3);
}
