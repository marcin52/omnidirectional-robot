#include "src/encoders.h"
#include "C:/Ruby27-x64/lib/ruby/gems/2.7.0/gems/ceedling-0.31.0/vendor/unity/src/unity.h"




































EncoderSoftware_t encoder = {0};

void setUp(void)

{

 memset(&encoder, sizeof(encoder), 0);

}



void tearDown(void)

{



}









void test_encodersPositive() {



 uint32_t prev = encoder.previousPosition = 0;

 uint32_t curr = encoder.currentPosition = 10;



 int32_t difference = Encoders_GetDifference(curr, prev);



 UnityAssertEqualNumber((UNITY_INT)((10)), (UNITY_INT)((difference)), (

((void *)0)

), (UNITY_UINT)(52), UNITY_DISPLAY_STYLE_INT);

}



void test_encodersNegative() {



 uint32_t prev = encoder.previousPosition = 10;

 uint32_t curr = encoder.currentPosition = 7;



 int32_t difference = Encoders_GetDifference(curr, prev);



 UnityAssertEqualNumber((UNITY_INT)((-3)), (UNITY_INT)((difference)), (

((void *)0)

), (UNITY_UINT)(62), UNITY_DISPLAY_STYLE_INT);

}



void test_encodersNegativeBorderCase() {



 uint32_t prev = encoder.previousPosition = 0;

 uint32_t curr = encoder.currentPosition = 

                                          0xffffffffU 

                                                     - 5;



 int32_t difference = Encoders_GetDifference(curr, prev);



 UnityAssertEqualNumber((UNITY_INT)((-6)), (UNITY_INT)((difference)), (

((void *)0)

), (UNITY_UINT)(72), UNITY_DISPLAY_STYLE_INT);

}



void test_encodersPositiveBorderCase() {



 uint32_t prev = encoder.previousPosition = 

                                           0xffffffffU 

                                                      - 1;

 uint32_t curr = encoder.currentPosition = 2;



 int32_t difference = Encoders_GetDifference(curr, prev);



 UnityAssertEqualNumber((UNITY_INT)((4)), (UNITY_INT)((difference)), (

((void *)0)

), (UNITY_UINT)(82), UNITY_DISPLAY_STYLE_INT);

}
