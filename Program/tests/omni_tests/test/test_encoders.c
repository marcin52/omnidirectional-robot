/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

 //-- unity: unit test framework
#include "unity.h"

//-- module being tested: 
#include "encoders.h"

// mocks and spies


//-- external libraries
#include <string.h>
#include <stdlib.h>

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

EncoderSoftware_t encoder = {0};

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    SETUP, TEARDOWN
 ******************************************************************************/

void setUp(void)
{
	memset(&encoder, sizeof(encoder), 0);
}

void tearDown(void)
{

}
/*******************************************************************************
 *    TESTS
 ******************************************************************************/

void test_encodersPositive() {

	uint32_t prev = encoder.previousPosition = 0;
	uint32_t curr = encoder.currentPosition = 10;

	int32_t difference = Encoders_GetDifference(curr, prev);

	TEST_ASSERT_EQUAL(10, difference);
}

void test_encodersNegative() {

	uint32_t prev = encoder.previousPosition = 10;
	uint32_t curr = encoder.currentPosition = 7;

	int32_t difference = Encoders_GetDifference(curr, prev);

	TEST_ASSERT_EQUAL(-3, difference);
}

void test_encodersNegativeBorderCase() {

	uint32_t prev = encoder.previousPosition = 0;
	uint32_t curr = encoder.currentPosition = UINT32_MAX - 5;

	int32_t difference = Encoders_GetDifference(curr, prev);

	TEST_ASSERT_EQUAL(-6, difference);
}

void test_encodersPositiveBorderCase() {

	uint32_t prev = encoder.previousPosition = UINT32_MAX - 1;
	uint32_t curr = encoder.currentPosition = 2;

	int32_t difference = Encoders_GetDifference(curr, prev);

	TEST_ASSERT_EQUAL(4, difference);
}
