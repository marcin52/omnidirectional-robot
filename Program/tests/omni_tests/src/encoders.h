#ifndef LIGHT_SCHEDULER_H
#define LIGHT_SCHEDULER_H

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

typedef struct{
	uint32_t pulsesPerRotation;
	uint32_t previousPosition;
	uint32_t currentPosition;
	uint16_t encA_PinNumber;
	uint16_t encB_PinNumber;
	bool A_state;
	bool B_state;
}EncoderSoftware_t;

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

int32_t Encoders_GetDifference(uint32_t prev, uint32_t curr);

#endif /* LIGHT_SCHEDULER_H */
