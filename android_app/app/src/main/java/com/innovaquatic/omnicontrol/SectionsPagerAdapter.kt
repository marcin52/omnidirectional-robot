package com.innovaquatic.omnicontrol

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class SectionsPagerAdapter(
    private val mContext: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getCount(): Int {
        return 1
    }

    override fun getItem(position: Int): Fragment {

        var fragment: Fragment?
        when (position) {
            0 -> fragment = MainControlFragment()
            else -> {

                fragment = MainControlFragment()
            }
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {

        when (position) {
            0 -> return mContext.resources.getString(R.string.main_control_tab)

            else -> {
                return null
            }
        }
    }
}
