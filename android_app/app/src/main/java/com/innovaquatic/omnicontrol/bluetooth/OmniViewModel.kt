package com.innovaquatic.omnicontrol.bluetooth

import androidx.lifecycle.ViewModel

class OmniViewModel: ViewModel() {

    var isRobotOn: Boolean = false

    var isGripperOn: Boolean = false

    var dof1Angle: Int = 90
    var dof2Angle: Int = 135
    var dof3Angle: Int = 135
    var dof4Angle: Int = 90
    var dof5Angle: Int = 90
    var dof6Angle: Int = 90
}
