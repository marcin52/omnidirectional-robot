package com.innovaquatic.omnicontrol.bluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.ContentValues
import android.content.Intent
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.os.AsyncTask
import android.view.View
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import com.innovaquatic.omnicontrol.R
import com.innovaquatic.omnicontrol.bluetooth.BluetoothSerial
import com.innovaquatic.omnicontrol.bluetooth.BluetoothSerialListener
import com.innovaquatic.omnicontrol.bluetooth.DialogBluetoothDeviceList
import java.io.IOException

class BluetoothCommService(
    private val activity: AppCompatActivity,
    private val bluetoothImage: ImageButton
):
    BluetoothSerialListener, DialogBluetoothDeviceList.OnDeviceSelectedListener {

    // Request code for enabling bluetooth activity intent
    private val ACTIVITY_REQUEST_CODE_ENABLE_BLUETOOTH = 1

    // Bluetooth variables
    private var isBluetoothConnected : Boolean = false
    private lateinit var bluetoothSerial : BluetoothSerial
    private var isBluetoothEnableRequestSended = false
    private var isConnectionDeviceRequested = false

    // Messages IDs
    private val MANIPULATOR_ON_OFF_ID = 11
    private val VELOCITY_VECTOR_ID = 12

    // Message config
    private val messageStartChar = '{'
    private val messageStopChar = '}'

    // dof numbers in robot hardware
    enum class DofNumbers(val number: Int){
        dof1Number(0), dof2Number(1), dof3Number(2),
        dof4Number(3), dof5Number(4), dof6Number(5)
    }

    init {

        bluetoothSerial = BluetoothSerial(activity, this)

        bluetoothImage.setOnClickListener {
            bluetoothImageOnClickListener()
        }
    }

    // Overriding functions
    override fun onBluetoothNotSupported() {
        showToast("Bluetooth is not supported on this device")
    }

    override fun onBluetoothDisabled() {
        if( ! isBluetoothEnableRequestSended) {
            val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity.startActivityForResult(enableBluetooth, ACTIVITY_REQUEST_CODE_ENABLE_BLUETOOTH)
            isBluetoothEnableRequestSended = true
        }
    }

    override fun onBluetoothDeviceDisconnected() {
        updateBluetoothState()
        if( isConnectionDeviceRequested ){
            showToast("disconnected from device")
            isConnectionDeviceRequested = false
        }
    }

    override fun onConnectingBluetoothDevice() {
        updateBluetoothState()
        showToast("connectiong to the device")
    }

    override fun onBluetoothDeviceConnected(name: String?, address: String?) {
        updateBluetoothState()
        showToast("Connected to $name")
    }

    override fun onBluetoothSerialRead(message: String?) {

    }

    override fun onBluetoothSerialWrite(message: String?) {

    }

    override fun onBluetoothDeviceSelected(device: BluetoothDevice?) {
        // Connect to the selected remote Bluetooth device
        bluetoothSerial.connect(device)
    }

    // Private functions
    private fun bluetoothImageOnClickListener(){

        when (getBluetoothState()) {

            BluetoothSerial.STATE_CONNECTED -> {
                bluetoothSerial.stop()
            }
            BluetoothSerial.STATE_DISCONNECTED -> {

                if (bluetoothSerial.isBluetoothEnabled) {
                    showDeviceListDialog()
                } else {
                    onBluetoothDisabled()
                }
            }
            else -> {
                bluetoothSerial.stop()
                showToast("bluetooth connection stopped")
            }
        }
    }

    private fun showDeviceListDialog() {
        // Display dialog for selecting a remote Bluetooth device
        isConnectionDeviceRequested = true
        val dialog = DialogBluetoothDeviceList(activity)
        dialog.setOnDeviceSelectedListener(this)
        dialog.setDevices(bluetoothSerial.pairedDevices)
        dialog.showAddress(true)
        dialog.show(activity.supportFragmentManager, "bluetooth device picker")
    }

    private fun getBluetoothState() : Int {
        return if (bluetoothSerial != null) bluetoothSerial.state else BluetoothSerial.STATE_DISCONNECTED
    }

    private fun updateBluetoothState(){

        when (getBluetoothState()) {
            BluetoothSerial.STATE_CONNECTING -> {
                isBluetoothConnected = false
                bluetoothImage.setImageDrawable(AppCompatResources.getDrawable(activity, R.drawable.icon_bluetooth_not_connected_white_100))
            }
            BluetoothSerial.STATE_CONNECTED -> {
                isBluetoothConnected = true
                bluetoothImage.setImageDrawable(AppCompatResources.getDrawable(activity, R.drawable.icon_bluetooth_connected_blue_100))
            }
            else -> {
                isBluetoothConnected = false
                bluetoothImage.setImageDrawable(AppCompatResources.getDrawable(activity, R.drawable.icon_bluetooth_not_connected_white_100))
            }
        }
    }

    private fun showToast(text: String){
        Toast.makeText(activity, text, Toast.LENGTH_LONG).show()
    }

    private fun bluetoothSend(msgID: Int, msgArray: Array<Byte>) { // msgValue array max size is 3

//        var dataToSend = ""
//        dataToSend += messageStartChar
//        dataToSend += msgID.toChar()
//
//        for (element in msgArray) {
//            dataToSend += element.toChar()
//        }
//
//        dataToSend += messageStopChar

        val arrayToSend = byteArrayOf(messageStartChar.toByte(), msgID.toByte(),
                msgArray[0], msgArray[1], msgArray[2],
                msgArray[3], messageStopChar.toByte())

        BluetoothSendTask(bluetoothSerial).execute(arrayToSend)
    }

    // Public functions

    fun setupSPPService(){
        bluetoothSerial.setup()
    }

    fun onEnableBluetoothActivityResult(requestCode: Int, resultCode: Int) {

        when (requestCode) {
            ACTIVITY_REQUEST_CODE_ENABLE_BLUETOOTH ->                 // Set up Bluetooth serial port when Bluetooth adapter is turned on
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    bluetoothSerial.setup()
                }
        }
    }

    fun sendRobotStartStop(startOrStop: Boolean){

        val intValue = if(startOrStop){
            1.toByte()
        } else {
            0.toByte()
        }

        val arrayToSend = arrayOf(intValue, 0, 0, 0)
        bluetoothSend(MANIPULATOR_ON_OFF_ID, arrayToSend)
    }

    fun sendRobotVelocityVector(w: Byte, v_x : Byte, v_y : Byte){

        val arrayToSend = arrayOf(w, v_x, v_y, 0)
        bluetoothSend(VELOCITY_VECTOR_ID, arrayToSend)
    }

    // Class to send bluetooth messages in non-blocking way
    private class BluetoothSendTask(private val bluetoothSerial : BluetoothSerial) : AsyncTask<ByteArray?, Void?, Boolean>() {

        override fun doInBackground(vararg p0: ByteArray?): Boolean {

            return try {
                bluetoothSerial.write(p0[0])
                true
            } catch (e: IOException){
                false
            }
        }
    }
}