package com.innovaquatic.omnicontrol

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import com.innovaquatic.omnicontrol.bluetooth.BluetoothCommService
import com.innovaquatic.omnicontrol.databinding.FragmentMainControlPositionBinding

class MainControlFragment : Fragment(), JoystickView.JoystickListener {

    private lateinit var binding: FragmentMainControlPositionBinding
    private lateinit var angleSeekbar : SeekBar
    private lateinit var joystick : JoystickView
    private lateinit var commService : BluetoothCommService
    private lateinit var textW : TextView
    private lateinit var textVX : TextView
    private lateinit var textVY : TextView

    private var mHandler: Handler = Handler()
    private var velVector : VelocityVec = VelocityVec()

    init {
       // sendMessage()
    }

    private fun sendMessage(){
        mHandler.postDelayed({

            //commService.sendRobotVelocityVector(velVector.w, velVector.v_x, velVector.v_y)

            textW.text = velVector.w.toString()
            textVX.text = velVector.v_x.toString()
            textVY.text = velVector.v_y.toString()

            sendMessage()
        }, 10)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMainControlPositionBinding.inflate(inflater, container, false)

        angleSeekbar = binding.angleSeekbar
        joystick = binding.joystick
        commService = (activity as MainActivity).commService
        joystick.setOnJoystickMovedListeners(this)

        textW = binding.text1
        textVX = binding.text2
        textVY = binding.text3

        initializeListeners()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    private fun initializeListeners(){

        val onSeekBarChangeListener = object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                seekbarHandleOnProgressChange(seekBar, i)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                seekBarHandleOnStopTrackingTouch(seekBar)
            }
        }

        binding.angleSeekbar.setOnSeekBarChangeListener(onSeekBarChangeListener)
    }

    private fun seekbarHandleOnProgressChange(seekBar: SeekBar, i: Int) {

        val w_value = i
        velVector.w = w_value.toByte()
        commService.sendRobotVelocityVector(velVector.w, velVector.v_x, velVector.v_y)
        textW.text = velVector.w.toString()
        //commService.sendRobotVelocityVector(w_value, 0, 0)
    }

    private fun seekBarHandleOnStopTrackingTouch(seekBar: SeekBar) {

        seekBar.progress = 64
    }

    override fun onJoystickMoved(xPercent: Float, yPercent: Float, id: Int) {

        //velVector.v_x = (63*(xPercent+1.0)).toInt().toByte()
        //velVector.v_y = (63*(yPercent+1.0)).toInt().toByte()
        velVector.v_x = (127*(xPercent)).toInt().toByte()
        velVector.v_y = (127*(yPercent)).toInt().toByte()
        commService.sendRobotVelocityVector(velVector.w, velVector.v_x, velVector.v_y)

        textVX.text = velVector.v_x.toString()
        textVY.text = velVector.v_y.toString()
        //textVX.text = (127 * xPercent).toInt().toString()
        //textVY.text = (127 * yPercent).toInt().toString()
       // commService.sendRobotVelocityVector(64, (127*xPercent).toInt(), (127*yPercent).toInt())

    }

    class VelocityVec{
        var w : Byte= 0
        var v_x : Byte = 0
        var v_y : Byte = 0
    }

}