package com.innovaquatic.omnicontrol

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.innovaquatic.omnicontrol.bluetooth.BluetoothCommService
import com.innovaquatic.omnicontrol.bluetooth.OmniViewModel
import com.innovaquatic.omnicontrol.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : AppCompatActivity(){

    private lateinit var binding: ActivityMainBinding
    lateinit var commService: BluetoothCommService

    lateinit var mViewModel: OmniViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mViewModel = ViewModelProviders.of(this).get(OmniViewModel::class.java)
        commService = BluetoothCommService(this, binding.bluetoothButton)

        restoreScreenData()

        binding.linearUp.robot_start_button.setOnClickListener{
            robotStartButtonOnClick()
        }

        val pageAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val pager: ViewPager = findViewById(R.id.contentFrame)
        pager.adapter = pageAdapter

        val tabLayout: TabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(pager)
    }

    override fun onStart() {
        super.onStart()
        commService.setupSPPService()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        commService.onEnableBluetoothActivityResult(requestCode, resultCode)
    }

    private fun robotStartButtonOnClick() {
        if( mViewModel.isRobotOn ){
            commService.sendRobotStartStop(false)
            binding.robotStartButton.setText("ROBOT_OFF")
            mViewModel.isRobotOn = false
        } else {
            commService.sendRobotStartStop(true)
            binding.robotStartButton.setText("ROBOT_ON")
            mViewModel.isRobotOn = true
        }
    }

    private fun restoreScreenData(){



    }
}